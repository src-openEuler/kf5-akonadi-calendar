%global framework akonadi-calendar

Name:           kf5-%{framework}
Version:        23.08.5
Release:        2
Summary:        The Akonadi Calendar Library

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/release-service/%{version}/src/%{framework}-%{version}.tar.xz

# handled by qt5-srpm-macros, which defines %%qt5_qtwebengine_arches
%{?qt5_qtwebengine_arches:ExclusiveArch: %{qt5_qtwebengine_arches}}

BuildRequires:  cyrus-sasl-devel
BuildRequires:  extra-cmake-modules
BuildRequires:  cmake(KF5TextEditTextToSpeech)
BuildRequires:  cmake(KPim5Libkdepim)
BuildRequires:  cmake(KPim5MailTransport)
BuildRequires:  cmake(KPim5Mime)
BuildRequires:  cmake(KPim5MessageCore)
BuildRequires:  grantlee-qt5-devel
BuildRequires:  kf5-rpm-macros

%global kf5_ver 5.87.0

BuildRequires:  kf5-kdelibs4support-devel >= %{kf5_ver}
BuildRequires:  kf5-kio-devel >= %{kf5_ver}
BuildRequires:  kf5-kwallet-devel >= %{kf5_ver}
BuildRequires:  kf5-kcodecs-devel >= %{kf5_ver}

#global majmin_ver %(echo %{version} | cut -d. -f1,2)
%global majmin_ver %{version}

BuildRequires:  kf5-grantleetheme-devel >= %{majmin_ver}
BuildRequires:  kf5-kmailtransport-devel >= %{majmin_ver}
BuildRequires:  kf5-kcontacts-devel >= %{majmin_ver}
BuildRequires:  kf5-kidentitymanagement-devel >= %{majmin_ver}
BuildRequires:  kf5-kcalendarcore-devel
BuildRequires:  kf5-kcalendarutils-devel >= %{majmin_ver}
BuildRequires:  kf5-akonadi-contacts-devel >= %{majmin_ver}
BuildRequires:  kf5-akonadi-server-devel >= %{majmin_ver}
BuildRequires:  qt5-qtbase-devel

BuildRequires:  kf5-akonadi-server >= %{majmin_ver}
BuildRequires:  kf5-akonadi-server-mysql
BuildRequires:  dbus-x11
BuildRequires:  xorg-x11-server-Xvfb

# have choice for libxtables.so.12
BuildRequires:  iptables-libs

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kf5-akonadi-contacts-devel
Requires:       kf5-akonadi-server-devel
Requires:       kf5-kcalendarcore-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build

%install
%cmake_install
%find_lang %{name} --all-name --with-html

%check
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
make test ARGS="--output-on-failure --timeout 30" -C %{_target_platform} ||:

%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_bindir}/kalendarac
%{_kf5_datadir}/akonadi/plugins/serializer/
%{_kf5_datadir}/dbus-1/services/org.kde.kalendarac.service
%{_kf5_datadir}/knotifications5/kalendarac.notifyrc
%{_kf5_datadir}/qlogging-categories5/*%{framework}.*
%{_kf5_datadir}/qlogging-categories5/org_kde_kalendarac.categories
%{_kf5_libdir}/libKPim5AkonadiCalendar.so.*
%{_kf5_qtplugindir}/akonadi_serializer_kcalcore.so
%{_kf5_qtplugindir}/kf5/org.kde.kcalendarcore.calendars/libakonadicalendarplugin.so
%{_kf5_sysconfdir}/xdg/autostart/org.kde.kalendarac.desktop

%files devel
%{_kf5_archdatadir}/mkspecs/modules/qt_AkonadiCalendar.pri
%{_includedir}/KPim5/AkonadiCalendar/Akonadi/
%{_includedir}/KPim5/AkonadiCalendar/akonadi-calendar_version.h
%{_includedir}/KPim5/AkonadiCalendar/akonadi/
%{_kf5_libdir}/cmake/KF5AkonadiCalendar/
%{_kf5_libdir}/cmake/KPim5AkonadiCalendar/
%{_kf5_libdir}/libKPim5AkonadiCalendar.so

%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Wed Jan 10 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 23.08.4-1
- Update package to version 23.08.4

* Fri Aug 04 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 23.04.3-1
- Update package to version 23.04.3

* Wed May 31 2023 misaka00251 <liuxin@iscas.ac.cn> - 22.12.0-1
- Init package
